FROM python:3.8-alpine3.14


ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app

COPY . /app

RUN pip install -r requirements.txt && apk --update --no-cache add curl

CMD python3 server.py